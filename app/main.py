import wikipedia
import re

from flask import Flask, request
from flask.views import MethodView

app = Flask(__name__)

@app.errorhandler(500)
def internal_error(error):
    return "Cannot retrieve what you are looking for right now"

class Request(MethodView):
    def post(self):
        outcome = request.json['outcome']
        answer = "I don't know what you are looking for." #DEFAULT
        if 'entities' in outcome:
            if 'wikipedia_search_query' in outcome['entities']:
                if len(outcome['entities']['wikipedia_search_query']) > 0:
                    if 'value' in outcome['entities']['wikipedia_search_query'][0]:
                        query = outcome['entities']['wikipedia_search_query'][0]['value']
                        query = re.sub('whos', '', query)
                        query = re.sub('who\'s', '', query)
                        query = re.sub('who is', '', query)
                        query = re.sub('what\'s the', '', query)
                        query = re.sub('whats the', '', query)
                        query = re.sub('what is the', '', query)
                        query = re.sub('what\'s', '', query)
                        query = re.sub('whats', '', query)
                        query = re.sub('what', '', query)

                        if request.json['lang'] == 'fr':
                            wikipedia.set_lang("fr")
                        elif request.json['lang'] == 'en':
                            wikipedia.set_lang("en")
                        wiki_answer = wikipedia.summary(query, sentences=1)
                        wiki_answer = re.sub(r'\[.*?\]', '', wiki_answer) #Remove notes
                        wiki_answer = re.sub(r'\(.*?\)', '', wiki_answer) #Remove parentheses
                        wiki_answer = re.sub(' +',' ', wiki_answer) #Remove multiple spaces
                        wiki_answer = re.sub('\"','', wiki_answer) #Remove quotes
                        answer = wiki_answer
                        answer += "."
            return answer

if __name__ == '__main__':
    app.add_url_rule('/', view_func=Request.as_view('request'))
    app.run(host='0.0.0.0', port=80)
